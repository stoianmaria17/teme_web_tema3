
const FIRST_NAME = "Maria";
const LAST_NAME = "Stoian";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
class Employee 
{
    constructor(name,surname,salary)
    {
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }
    getDetails()
    {
        return `${this.name} ${this.surname} ${this.salary}`;
    }
    
}
class SoftwareEngineer extends Employee{
   constructor(name,surname,salary,experience='JUNIOR')
   {
    super(name,surname,salary);
    this.experience=experience;    
   }
   applyBonus()
   { 
       
     /* switch(this.experience)
      {
          case 'MIDDLE': this.salary+=0.15*this.salary;break;
          case 'SENIOR':this.salary+=0.2*this.salary;break;
          default: this.salary+=0.1*this.salary;break;
      }
    return this.salary;*/
    var sal=this.salary;
    switch(this.experience)
      {
          case 'MIDDLE': sal+=0.15*sal;break;
          case 'SENIOR':sal+=0.2*sal;break;
          default:sal+=0.1*sal;break;
      }
    return sal;
}
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

